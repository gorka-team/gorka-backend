package me.antandtim.gorka.testcommons.testservice

data class TestDto(
        var id: Long? = null,
        var name: String? = null
)
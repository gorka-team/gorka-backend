package me.antandtim.gorka.event

import me.antandtim.gorka.event.Messages.exceededMessage
import me.antandtim.gorka.event.Messages.reminderMessage
import me.antandtim.gorka.payment.model.Payment
import me.antandtim.gorka.payment.service.PaymentService
import me.antandtim.gorka.telegram.config.GorkaBot
import me.antandtim.gorka.telegram.service.TelegramChatService
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import java.time.Instant

@Service
class PaymentNotifier(
    private val paymentService: PaymentService,
    private val gorkaBot: GorkaBot,
    private val telegramChatService: TelegramChatService
) {
    
    fun notifyPayment() {
        processNearlyExceeded()
        processExceeded()
    }
    
    private fun processNearlyExceeded() =
        paymentService.findPaymentsLessThanDayLeft().forEach { payment ->
            payment.amount?.let {
                processPayment(
                    payment,
                    reminderMessage(it)
                )
            }
        }
    
    private fun processExceeded() =
        paymentService.findExceededPayments().forEach { payment ->
            payment.amount?.let {
                processPayment(
                    payment,
                    exceededMessage(it)
                )
            }
        }
    
    private fun processPayment(payment: Payment, messageToSend: String) {
        payment
            .sportsman
            ?.alias
            ?.let { alias -> telegramChatService.findByAlias(alias) }
            ?.chatId
            ?.let { chatId: Long ->
                gorkaBot.execute(
                    SendMessage()
                        .setChatId(chatId)
                        .setText(messageToSend)
                )
                paymentService.update(payment.apply {
                    wasNotified = true
                    lastNotification = Instant.now()
                })
            }
    }
}
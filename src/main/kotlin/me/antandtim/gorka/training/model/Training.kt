package me.antandtim.gorka.training.model

import me.antandtim.gorka.sportsman.model.Sportsman
import java.time.Instant
import javax.persistence.*

@Entity
class Training {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trainingIdGenerator")
    @SequenceGenerator(name = "trainingIdGenerator", sequenceName = "training_seq")
    var id: Long? = null

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "SPORTSMAN_TRAINING",
        joinColumns = [JoinColumn(name = "TRAININGS_ID")],
        inverseJoinColumns = [JoinColumn(name = "SPORTSMAN_ID")]
    )
    var sportsmen: List<Sportsman>? = null

    var startTime: Instant? = null

    var notes: String? = null
}
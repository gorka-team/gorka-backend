package me.antandtim.gorka.telegram.model

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class TelegramChat {
    
    @Id
    var chatId: Long? = null
    
    var alias: String? = null
}
package me.antandtim.gorka

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.testcontainers.containers.PostgreSQLContainer
import uk.co.jemos.podam.api.PodamFactory
import uk.co.jemos.podam.api.PodamFactoryImpl

@AutoConfigureMockMvc
@SpringBootTest(classes = [GorkaScan::class])
@ComponentScan(basePackageClasses = [GorkaScan::class])
@ExtendWith(SpringExtension::class, TelegramBotContextInitializer::class)
@ContextConfiguration(initializers = [AbstractSpringTest.Initializer::class])
abstract class AbstractSpringTest {
    
    @Autowired
    protected lateinit var mockMvc: MockMvc
    
    companion object {
        private val FACTORY: PodamFactory = PodamFactoryImpl()
        
        protected fun <T> getMockObject(clazz: Class<T>?): T = FACTORY.manufacturePojo(clazz)
        
        internal val container = PostgreSQLContainer<Nothing>("postgres:13.2").apply {
            withDatabaseName("db")
            withUsername("user")
            withPassword("password")
        }
    }
    
    internal class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
        override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
            container.start()
            TestPropertyValues.of(
                "spring.datasource.url=" + container.jdbcUrl,
                "spring.datasource.username=" + container.username,
                "spring.datasource.password=" + container.password
            ).applyTo(configurableApplicationContext.environment)
        }
    }
}
package me.antandtim.gorka.news.controller

import me.antandtim.gorka.common.controller.CrudController
import me.antandtim.gorka.common.controller.DefaultCrudController
import me.antandtim.gorka.common.model.CountableResponse
import me.antandtim.gorka.news.mapper.NewsMapper
import me.antandtim.gorka.news.model.NewsDto
import me.antandtim.gorka.news.service.NewsService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/news")
class NewsController(
    newsService: NewsService,
    newsMapper: NewsMapper
) : CrudController<NewsDto> {
    
    private val defaultCrudController = DefaultCrudController(newsService, newsMapper)
    
    override fun getList(ids: List<Long>?): CountableResponse<List<NewsDto>> {
        return defaultCrudController.getList(ids)
    }
    
    override fun getOne(id: Long): NewsDto {
        return defaultCrudController.getOne(id)
    }
    
    override fun create(dto: NewsDto): NewsDto {
        return defaultCrudController.create(dto)
    }
    
    override fun update(dto: NewsDto, id: String): NewsDto {
        return defaultCrudController.update(dto, id)
    }
    
    override fun delete(id: Long): NewsDto {
        return defaultCrudController.delete(id)
    }
}
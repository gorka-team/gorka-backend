package me.antandtim.gorka.telegram.service

import me.antandtim.gorka.common.service.DefaultCrudService
import me.antandtim.gorka.telegram.model.TelegramChat
import me.antandtim.gorka.telegram.repository.TelegramChatRepository
import org.springframework.stereotype.Service

@Service
class TelegramChatService(private val telegramChatRepository: TelegramChatRepository) :
    DefaultCrudService<TelegramChatRepository, TelegramChat> {
    
    override fun repository(): TelegramChatRepository {
        return telegramChatRepository
    }
    
    fun findByAlias(alias: String): TelegramChat? {
        return telegramChatRepository.findByAlias(alias)
    }
}
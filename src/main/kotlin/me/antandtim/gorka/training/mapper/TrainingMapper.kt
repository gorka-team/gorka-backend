package me.antandtim.gorka.training.mapper

import me.antandtim.gorka.common.mapper.EntityMapper
import me.antandtim.gorka.sportsman.model.Sportsman
import me.antandtim.gorka.sportsman.repository.SportsmanRepository
import me.antandtim.gorka.training.model.Training
import me.antandtim.gorka.training.model.TrainingDto
import me.antandtim.gorka.training.model.TrainingListDto
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired

@Mapper(componentModel = "spring")
abstract class TrainingMapper : EntityMapper<Training, TrainingDto> {

    @Autowired
    private lateinit var sportsmanRepository: SportsmanRepository

    @JvmName("mapFromSportsmen")
    fun map(value: List<Sportsman>): List<Long> {
        return value.mapNotNull { it.id }
    }

    @JvmName("mapToSportsmen")
    fun map(value: List<Long>): List<Sportsman> {
        return sportsmanRepository.findAllById(value)
    }

    abstract fun toListDto(training: Training): TrainingListDto

    fun mapSportsmanToAliases(value: List<Sportsman>): List<String> {
        return value.map { it.alias }
    }
}
package me.antandtim.gorka.sportsman.model

import javax.validation.constraints.Pattern

class SportsmanDto {
    
    var id: Long? = null
    
    var fullName: String = ""
    
    var alias: String = ""
    
    var notes: String = ""
    
    @Pattern(regexp = "\\+\\d{1,3}\\d{10}")
    var phone: String = ""
    
    var trainings: List<Long>? = null
}
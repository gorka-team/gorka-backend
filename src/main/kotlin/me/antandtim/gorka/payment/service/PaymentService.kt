package me.antandtim.gorka.payment.service

import me.antandtim.gorka.common.service.DefaultCrudService
import me.antandtim.gorka.payment.model.Payment
import me.antandtim.gorka.payment.repository.PaymentRepository
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.temporal.ChronoUnit

@Service
class PaymentService(
    private val paymentRepository: PaymentRepository
) : DefaultCrudService<PaymentRepository, Payment> {
    
    override fun repository() = paymentRepository
    
    fun findPaymentsLessThanDayLeft(): List<Payment> = Instant.now().let {
        return repository().findAllByIsPaidFalseAndPaymentDateBetweenAndWasNotifiedFalse(
            it,
            it.plus(1, ChronoUnit.DAYS)
        )
    }
    
    fun findExceededPayments() =
        repository().findAllByIsPaidFalseAndPaymentDateBeforeAndLastNotificationBefore()
}
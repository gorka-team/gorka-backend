package me.antandtim.gorka.event

object Messages {
    
    fun reminderMessage(amount: Double) = """
        Здарова, бродяга! У тебя меньше суток на оплату.
        Напомню, ты должен заплатить $amount рубасов.
        Не разочаровывай, а то я разозлюсь :)
    """.trimIndent()
    
    fun exceededMessage(amount: Double) = """
        Здарова, бродяга! Ты просрал платеж.
        Напомню, ты должен заплатить $amount рубасов.
        Плати давай! Или ты меня не уважаешь? Смотри, я ведь и расстроиться могу...
    """.trimIndent()
}
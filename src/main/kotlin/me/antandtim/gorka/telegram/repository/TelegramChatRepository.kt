package me.antandtim.gorka.telegram.repository

import me.antandtim.gorka.telegram.model.TelegramChat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TelegramChatRepository : JpaRepository<TelegramChat, Long> {
    
    fun findByAlias(alias: String): TelegramChat?
}
package me.antandtim.gorka.payment.controller

import com.fasterxml.jackson.databind.ObjectMapper
import me.antandtim.gorka.AbstractSpringTest
import me.antandtim.gorka.config.dev.DevDbInit
import me.antandtim.gorka.payment.model.PaymentDto
import me.antandtim.gorka.payment.service.PaymentService
import me.antandtim.gorka.sportsman.service.SportsmanService
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.time.Instant
import java.time.temporal.ChronoUnit

internal class PaymentControllerTest : AbstractSpringTest() {
    
    @Autowired
    private lateinit var objectMapper: ObjectMapper
    
    @Autowired
    private lateinit var sportsmanService: SportsmanService
    
    @Autowired
    private lateinit var paymentService: PaymentService
    
    @BeforeEach
    fun setup() {
        kotlin.runCatching { sportsmanService.delete(1) }
        sportsmanService.create(DevDbInit.sportsman(emptyList(), emptyList()))
    }
    
    @Test
    fun getList() {
        sportsmanService.getOne(1)?.let {
            val payment = paymentService.create(DevDbInit.payment(it))
            
            mockMvc
                .perform(get(URL_TEMPLATE))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.content.[*].id").value(hasItem(payment.id?.toInt())))
                .andExpect(jsonPath("$.content.[*].isPaid").value(hasItem(false)))
                .andExpect(jsonPath("$.content.[*].amount").value(hasItem(3200.0)))
                .andExpect(jsonPath("$.content.[*].sportsmanId").value(hasItem(1)))
        }
    }
    
    @Test
    fun getOne() {
        sportsmanService.getOne(1)?.let {
            val payment = paymentService.create(DevDbInit.payment(it))
            
            mockMvc
                .perform(get("$URL_TEMPLATE/${payment.id}"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("id").value(payment.id))
                .andExpect(jsonPath("isPaid").value(false))
                .andExpect(jsonPath("sportsmanId").value(1))
        }
    }
    
    @Test
    fun create() {
        mockMvc
            .perform(
                post(URL_TEMPLATE)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(paymentDto()))
            )
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("isPaid").value(false))
            .andExpect(jsonPath("amount").value(3200.0))
            .andExpect(jsonPath("sportsmanId").value(1))
    }
    
    @Test
    fun update() {
        sportsmanService.getOne(1)?.let {
            val payment = paymentService.create(DevDbInit.payment(it))
            
            mockMvc
                .perform(
                    put("$URL_TEMPLATE/${payment}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(paymentDto().apply {
                            id = payment.id
                            amount = 2000.0
                        }))
                )
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("id").value(payment.id))
                .andExpect(jsonPath("isPaid").value(false))
                .andExpect(jsonPath("amount").value(2000.0))
                .andExpect(jsonPath("sportsmanId").value(1))
        }
    }
    
    @Test
    fun delete() {
        sportsmanService.getOne(1)?.let {
            val payment = paymentService.create(DevDbInit.payment(it))
            
            mockMvc
                .perform(delete("$URL_TEMPLATE/${payment.id}"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("id").value(payment.id))
                .andExpect(jsonPath("isPaid").value(false))
                .andExpect(jsonPath("amount").value(3200.0))
                .andExpect(jsonPath("sportsmanId").value(1))
        }
    }
    
    companion object {
        const val URL_TEMPLATE = "/payment"
        
        fun paymentDto() = PaymentDto().apply {
            isPaid = false
            paymentDate = Instant.now().plus(1, ChronoUnit.HOURS)
            amount = 3200.0
            this.sportsmanId = 1
        }
    }
}
package me.antandtim.gorka.sportsman.repository

import me.antandtim.gorka.sportsman.model.Sportsman
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SportsmanRepository : JpaRepository<Sportsman, Long>
package me.antandtim.gorka.telegram.command

import me.antandtim.gorka.telegram.command.CommandEnum.CommandName.start

enum class CommandEnum(val commandName: String) {
    START(start);
    
    object CommandName {
        const val start = "/start"
    }
}
package me.antandtim.gorka.testcommons.testservice

import me.antandtim.gorka.common.mapper.EntityMapper
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired

@Mapper(componentModel = "spring")
abstract class TestMapper : EntityMapper<TestEntity, TestDto> {

    @Autowired
    private lateinit var repo: TestRepository

}
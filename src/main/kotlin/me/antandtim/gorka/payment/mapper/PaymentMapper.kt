package me.antandtim.gorka.payment.mapper

import me.antandtim.gorka.common.mapper.EntityMapper
import me.antandtim.gorka.payment.model.Payment
import me.antandtim.gorka.payment.model.PaymentDto
import me.antandtim.gorka.sportsman.repository.SportsmanRepository
import org.mapstruct.AfterMapping
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.springframework.beans.factory.annotation.Autowired

@Mapper(componentModel = "spring")
abstract class PaymentMapper : EntityMapper<Payment, PaymentDto> {
    
    @Autowired
    private lateinit var sportsmanRepository: SportsmanRepository
    
    @AfterMapping
    fun toDto(@MappingTarget paymentDto: PaymentDto, payment: Payment) {
        paymentDto.sportsmanId = payment.sportsman?.id
    }
    
    @AfterMapping
    fun fromDto(@MappingTarget payment: Payment, paymentDto: PaymentDto) {
        paymentDto.sportsmanId?.let {
            payment.sportsman = sportsmanRepository.findById(it).orElse(null)
        }
    }
}
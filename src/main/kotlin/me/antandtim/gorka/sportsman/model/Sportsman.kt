package me.antandtim.gorka.sportsman.model

import me.antandtim.gorka.payment.model.Payment
import me.antandtim.gorka.training.model.Training
import javax.persistence.*

@Entity
class Sportsman {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sportsmanIdGenerator")
    @SequenceGenerator(name = "sportsmanIdGenerator", sequenceName = "sportsman_seq")
    var id: Long? = null
    
    var fullName: String = ""
    
    var alias: String = ""
    
    var notes: String = ""
    
    var phone: String = ""
    
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "sportsmen")
    var trainings: List<Training>? = null
    
    @OneToMany(mappedBy = "sportsman")
    var payments: List<Payment>? = null
}
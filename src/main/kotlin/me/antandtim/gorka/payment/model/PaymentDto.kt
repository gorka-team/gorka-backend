package me.antandtim.gorka.payment.model

import java.time.Instant

class PaymentDto {
    
    var id: Long? = null
    
    var isPaid: Boolean? = null
    
    var paymentDate: Instant? = null
    
    var amount: Double? = null
    
    var sportsmanId: Long? = null
}
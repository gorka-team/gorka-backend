package me.antandtim.gorka.payment.controller

import me.antandtim.gorka.common.controller.CrudController
import me.antandtim.gorka.common.controller.DefaultCrudController
import me.antandtim.gorka.common.model.CountableResponse
import me.antandtim.gorka.payment.mapper.PaymentMapper
import me.antandtim.gorka.payment.model.PaymentDto
import me.antandtim.gorka.payment.service.PaymentService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/payment")
class PaymentController(
    paymentService: PaymentService,
    paymentMapper: PaymentMapper
) : CrudController<PaymentDto> {
    
    private val defaultCrudController = DefaultCrudController(paymentService, paymentMapper)
    
    override fun getList(ids: List<Long>?): CountableResponse<List<PaymentDto>> {
        return defaultCrudController.getList(ids)
    }
    
    override fun getOne(id: Long): PaymentDto {
        return defaultCrudController.getOne(id)
    }
    
    override fun create(dto: PaymentDto): PaymentDto {
        return defaultCrudController.create(dto)
    }
    
    override fun update(dto: PaymentDto, id: String): PaymentDto {
        return defaultCrudController.update(dto, id)
    }
    
    override fun delete(id: Long): PaymentDto {
        return defaultCrudController.delete(id)
    }
}
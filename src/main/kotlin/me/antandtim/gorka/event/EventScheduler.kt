package me.antandtim.gorka.event

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class EventScheduler(
    private val paymentNotifier: PaymentNotifier
) {
    
    // We can do it in a much more efficient way by scheduling with the basis of next paymentDate
    // But i'm kinda lazy to do it. Moreover, considering new payments may appear, so we have to
    // handle it in the scheduler. Thus, too hard, just check every second.
    @Scheduled(fixedDelay = 1000)
    fun schedule() {
        paymentNotifier.notifyPayment()
    }
}
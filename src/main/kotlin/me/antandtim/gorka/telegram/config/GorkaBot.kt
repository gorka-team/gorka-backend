package me.antandtim.gorka.telegram.config

import me.antandtim.telegram.api.service.TelegramHandler
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.BotApiMethod
import org.telegram.telegrambots.meta.api.objects.Update
import java.io.Serializable

@Component
class GorkaBot(
    private val config: BotConfig,
    private val telegramHandler: TelegramHandler
) : TelegramLongPollingBot() {
    
    override fun getBotToken(): String {
        return config.token
    }
    
    override fun getBotUsername(): String {
        return config.name
    }
    
    override fun onUpdateReceived(update: Update?) {
        update?.apply {
            telegramHandler.handle(this) { message: BotApiMethod<Serializable> ->
                execute(message)
            }
        }
    }
    
}
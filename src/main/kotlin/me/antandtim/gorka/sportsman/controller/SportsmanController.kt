package me.antandtim.gorka.sportsman.controller

import me.antandtim.gorka.common.controller.CrudController
import me.antandtim.gorka.common.controller.DefaultCrudController
import me.antandtim.gorka.common.model.CountableResponse
import me.antandtim.gorka.sportsman.mapper.SportsmanMapper
import me.antandtim.gorka.sportsman.model.SportsmanDto
import me.antandtim.gorka.sportsman.service.SportsmanService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/sportsman")
class SportsmanController(
    sportsmanService: SportsmanService,
    sportsmanMapper: SportsmanMapper
) : CrudController<SportsmanDto> {

    private val defaultCrudController = DefaultCrudController(sportsmanService, sportsmanMapper)

    override fun getList(ids: List<Long>?): CountableResponse<List<SportsmanDto>> {
        return defaultCrudController.getList(ids)
    }

    override fun getOne(id: Long): SportsmanDto {
        return defaultCrudController.getOne(id)
    }

    override fun create(dto: SportsmanDto): SportsmanDto {
        return defaultCrudController.create(dto)
    }

    override fun update(dto: SportsmanDto, id: String): SportsmanDto {
        return defaultCrudController.update(dto, id)
    }

    override fun delete(id: Long): SportsmanDto {
        return defaultCrudController.delete(id)
    }
}
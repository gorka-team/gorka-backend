package me.antandtim.gorka.common.model

data class CountableResponse<T>(

    val totalElements: Long,

    val content: T
)
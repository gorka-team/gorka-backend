# GORKA

## Configuration Management

### Dependencies

You need to use Maven to build the project.
Google for installation instructions for your system.

### Maven Configuration

We use custom Maven artifacts.  
You need GitHub token to access it. 
Go to your GitHub account, Settings > Developer settings > Personal access tokens >
Generate new token > read:packages > Generate token.  
Save the generated token, you can't access it later.

Go to `settings.xml` at project root. 
Find GitHub server configuration, under `username` enter your GitHub username,
under password enter your token you get earlier.

NEVER EVER PUSH FILES WITH YOUR PERSONAL TOKENS!

Move `settings.xml` to `~/.m2/`, merge or replace if needed. 

### IntelliJ Configuration

Import project as maven project normally.

Go to IntelliJ settings, find "Annotation processors", 
ensure box "Enable Annotation Processing" is checked.

Stay in settings, go to "Build, Execution, Deployment" > Build Tools >
Maven > Runner and ensure that "Delegate IDE build/run actions to Maven".
Check "Skip Tests" if needed.
This is needed because IntelliJ can't build projects with annotation processing properly.

### Run project

Use Maven regular scripts to perform activities on the project.

`mvn clean install`
`mvn clean test`

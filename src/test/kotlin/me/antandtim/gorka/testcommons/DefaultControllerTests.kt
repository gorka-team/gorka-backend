package me.antandtim.gorka.testcommons

import me.antandtim.gorka.common.controller.DefaultCrudController
import me.antandtim.gorka.testcommons.testservice.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import kotlin.test.assertEquals
import kotlin.test.fail

class DefaultControllerTests {

    private val service = Mockito.mock(TestService::class.java)
    private val mapper = Mockito.mock(TestMapper::class.java)

    private val controller: DefaultCrudController<
            TestDto,
            TestEntity,
            TestService,
            TestRepository,
            TestMapper
            > = DefaultCrudController(service, mapper)

    private companion object {
        val ids = 1..3L
        val dummyDtos = ids.map { TestDto(it, "Aaa$it") }
        val dummyEntities = dummyDtos.map { TestEntity().apply { id = it.id; name = it.name } }
    }

    @BeforeEach
    fun setUpMocks() {
        Mockito.`when`(service.getList()).thenReturn(dummyEntities)
        for (i in ids) {
            Mockito.`when`(mapper.dto(dummyEntities[i.toInt() - 1]))
                    .thenReturn(dummyDtos[i.toInt() - 1])
            Mockito.`when`(service.getOne(i))
                    .thenReturn(dummyEntities[i.toInt() - 1])
        }
    }

    @Test
    fun testGetListNull() {
        controller.getList(null)

        Mockito.verify(service).getList()
        for (i in dummyEntities) {
            Mockito.verify(mapper).dto(i)
        }
    }

    @Test
    fun testGetListEmpty() {
        controller.getList(listOf())

        Mockito.verify(service).getList()
        for (i in dummyEntities) {
            Mockito.verify(mapper).dto(i)
        }
    }

    @Test
    fun testGetListSpecificOne() {
        val request = listOf<Long>(2)
        val dummyEntity = dummyEntities[request[0].toInt() - 1]

        Mockito.`when`(service.getMany(request)).thenReturn(listOf(dummyEntity))

        val result = controller.getList(request).content[0]
        assertCorrectDto(2, result)

        Mockito.verify(service).getMany(request)
        Mockito.verify(mapper).dto(dummyEntity)
    }

    @Test
    fun testGetListSpecificSeveral() {
        val request = listOf<Long>(3, 1)
        val answer = request.map { dummyEntities[it.toInt() - 1] }

        Mockito.`when`(service.getMany(request)).thenReturn(answer)

        val result = controller.getList(request).content
        for (i in result.indices) {
            assertCorrectDto(request[i], result[i])
        }

        Mockito.verify(service).getMany(request)
        for (i in request) {
            Mockito.verify(mapper).dto(dummyEntities[i.toInt() - 1])
        }
    }

    @Test
    fun testGetListNonExistent() {
        val request = listOf<Long>(3, 1, 100, 2)

        Mockito.`when`(service.getMany(request)).thenThrow(NoSuchElementException())

        try {
            controller.getList(request)
            // v----- Should be unreachable -----v
            fail("Exception should have been thrown.")
        } catch (ex: NoSuchElementException) {
        }
    }

    @Test
    fun testGetOne() {
        for (i in ids) {
            val result = controller.getOne(i)
            assertCorrectDto(i, result)

            Mockito.verify(service).getOne(i)
            Mockito.verify(mapper).dto(dummyEntities[i.toInt() - 1])
        }
    }

    @Test
    fun testGetOneNonExistent() {
        try {
            controller.getOne(42)
            // v----- Should be unreachable -----v
            fail("Exception should have been thrown.")
        } catch (ex: NoSuchElementException) {
        }
    }

    @Test
    fun testCreate() {
        val dummyDto = dummyDtos[0]
        val dummyEntity = dummyEntities[0]

        Mockito.`when`(mapper.entity(dummyDto)).thenReturn(dummyEntity)
        Mockito.`when`(service.create(dummyEntity)).thenReturn(dummyEntity)
        Mockito.`when`(mapper.dto(dummyEntity)).thenReturn(dummyDto)

        controller.create(dummyDto)

        Mockito.verify(mapper).entity(dummyDto)
        Mockito.verify(service).create(dummyEntity)
        Mockito.verify(mapper).dto(dummyEntity)
    }

    @Test
    fun testUpdate() {
        val dummyDto = dummyDtos[0]
        val dummyEntity = dummyEntities[0]

        Mockito.`when`(mapper.entity(dummyDto)).thenReturn(dummyEntity)
        Mockito.`when`(service.update(dummyEntity)).thenReturn(dummyEntity)
        Mockito.`when`(mapper.dto(dummyEntity)).thenReturn(dummyDto)

        controller.update(dummyDto, dummyDto.id.toString())

        Mockito.verify(mapper).entity(dummyDto)
        Mockito.verify(service).update(dummyEntity)
        Mockito.verify(mapper).dto(dummyEntity)
    }

    @Test
    fun testDelete() {
        val dummyDto = dummyDtos[0]
        val dummyEntity = dummyEntities[0]
        val id = dummyDto.id ?: 1

        Mockito.`when`(service.delete(id)).thenReturn(dummyEntity)
        Mockito.`when`(mapper.dto(dummyEntity)).thenReturn(dummyDto)

        controller.delete(id)

        Mockito.verify(service).delete(id)
        Mockito.verify(mapper).dto(dummyEntity)
    }

    @Test
    fun testDeleteNonExistent() {
        val id = 100L

        Mockito.`when`(service.delete(id)).thenThrow(NoSuchElementException())

        try {
            controller.delete(id)
            // v----- Should be unreachable -----v
            fail("Exception should have been thrown.")
        } catch (ex: NoSuchElementException) {
        }
    }

    private fun assertCorrectEntity(id: Long, entity: TestEntity) {
        assertEquals(id, entity.id)
        assertEquals("Aaa$id", entity.name)
    }

    private fun assertCorrectDto(id: Long, entity: TestDto) {
        assertEquals(id, entity.id)
        assertEquals("Aaa$id", entity.name)
    }
}
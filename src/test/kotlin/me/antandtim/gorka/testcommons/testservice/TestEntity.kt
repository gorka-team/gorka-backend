package me.antandtim.gorka.testcommons.testservice

import javax.persistence.*

@Entity
class TestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "testIdGenerator")
    @SequenceGenerator(name = "testIdGenerator", sequenceName = "test_seq")
    var id: Long? = null

    var name: String? = null
}
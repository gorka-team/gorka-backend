package me.antandtim.gorka

import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.telegram.telegrambots.ApiContextInitializer

class TelegramBotContextInitializer : BeforeAllCallback {
    
    override fun beforeAll(p0: ExtensionContext?) {
        ApiContextInitializer.init()
    }
}
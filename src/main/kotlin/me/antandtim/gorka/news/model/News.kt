package me.antandtim.gorka.news.model

import org.hibernate.annotations.CreationTimestamp
import java.time.Instant
import javax.persistence.*

@Entity
class News {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "newsIdGenerator")
    @SequenceGenerator(name = "newsIdGenerator", sequenceName = "news_seq")
    var id: Long? = null
    
    var title: String? = null
    
    var body: String? = null
    
    @CreationTimestamp
    var timestamp: Instant? = null
    
}
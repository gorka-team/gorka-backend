package me.antandtim.gorka.config.dev

import me.antandtim.gorka.payment.model.Payment
import me.antandtim.gorka.payment.service.PaymentService
import me.antandtim.gorka.sportsman.model.Sportsman
import me.antandtim.gorka.sportsman.service.SportsmanService
import me.antandtim.gorka.training.model.Training
import me.antandtim.gorka.training.service.TrainingService
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.time.Instant
import java.time.temporal.ChronoUnit

@Component
@Profile("dev")
class DevDbInit(
    private val sportsmanService: SportsmanService,
    private val paymentService: PaymentService,
    private val trainingService: TrainingService
) : InitializingBean {
    
    override fun afterPropertiesSet() {
        val sportsman = sportsmanService.create(sportsman(emptyList(), emptyList()))
        trainingService.create(training(sportsman))
        paymentService.create(payment(sportsman))
    }
    
    companion object {
        fun sportsman(trainings: List<Training>, payments: List<Payment>) =
            Sportsman().apply {
                fullName = "Anton Timchenko"
                alias = "AntAndTim"
                notes = "Very strong gay"
                phone = "+79999999999"
            }.also {
                it.trainings = trainings
                it.payments = payments
            }
        
        fun training(sportsman: Sportsman) = Training().apply {
            sportsmen = listOf(sportsman)
            startTime = Instant.now()
            notes = "Some gay training"
        }
        
        fun payment(sportsman: Sportsman) = Payment().apply {
            isPaid = false
            paymentDate = Instant.now().plus(1, ChronoUnit.HOURS)
            amount = 3200.0
            this.sportsman = sportsman
        }
    }
}
package me.antandtim.gorka

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.telegram.telegrambots.ApiContextInitializer
import springfox.documentation.swagger2.annotations.EnableSwagger2

@EnableSwagger2
@SpringBootApplication
class GorkaApplication

fun main(args: Array<String>) {
    ApiContextInitializer.init()
    SpringApplication.run(GorkaApplication::class.java, *args)
}

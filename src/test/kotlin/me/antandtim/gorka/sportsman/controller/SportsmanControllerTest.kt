package me.antandtim.gorka.sportsman.controller

import com.fasterxml.jackson.databind.ObjectMapper
import me.antandtim.gorka.AbstractSpringTest
import me.antandtim.gorka.config.dev.DevDbInit
import me.antandtim.gorka.sportsman.model.SportsmanDto
import me.antandtim.gorka.sportsman.service.SportsmanService
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

internal class SportsmanControllerTest : AbstractSpringTest() {
    
    @Autowired
    private lateinit var objectMapper: ObjectMapper
    
    @Autowired
    private lateinit var sportsmanService: SportsmanService
    
    @Test
    fun getList() {
        val sportsman = sportsmanService.create(DevDbInit.sportsman(emptyList(), emptyList()))
        mockMvc
            .perform(get(URL_TEMPLATE))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.content.[*].id").value(hasItem(sportsman.id?.toInt())))
            .andExpect(jsonPath("$.content.[*].fullName").value(hasItem(sportsman.fullName)))
            .andExpect(jsonPath("$.content.[*].alias").value(hasItem(sportsman.alias)))
            .andExpect(jsonPath("$.content.[*].notes").value(hasItem(sportsman.notes)))
            .andExpect(jsonPath("$.content.[*].phone").value(hasItem(sportsman.phone)))
        
    }
    
    @Test
    fun getOne() {
        val sportsman = sportsmanService.create(DevDbInit.sportsman(emptyList(), emptyList()))
        mockMvc
            .perform(get("$URL_TEMPLATE/${sportsman.id}"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("id").value(sportsman.id?.toInt()))
            .andExpect(jsonPath("fullName").value(sportsman.fullName))
            .andExpect(jsonPath("alias").value(sportsman.alias))
            .andExpect(jsonPath("notes").value(sportsman.notes))
            .andExpect(jsonPath("phone").value(sportsman.phone))
    }
    
    @Test
    fun create() {
        val sportsmanDto = sportsmanDto()
        mockMvc
            .perform(
                post(URL_TEMPLATE)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(sportsmanDto))
            )
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("fullName").value(sportsmanDto.fullName))
            .andExpect(jsonPath("alias").value(sportsmanDto.alias))
            .andExpect(jsonPath("notes").value(sportsmanDto.notes))
            .andExpect(jsonPath("phone").value(sportsmanDto.phone))
    }
    
    @Test
    fun update() {
        val sportsman = sportsmanService.create(DevDbInit.sportsman(emptyList(), emptyList()))
        
        val sportsmanDto = sportsmanDto().apply {
            id = sportsman.id
            alias = "Alias"
        }
        
        mockMvc
            .perform(
                put("$URL_TEMPLATE/${sportsman.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(sportsmanDto))
            )
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("id").value(sportsman.id?.toInt()))
            .andExpect(jsonPath("fullName").value(sportsmanDto.fullName))
            .andExpect(jsonPath("alias").value("Alias"))
            .andExpect(jsonPath("notes").value(sportsmanDto.notes))
            .andExpect(jsonPath("phone").value(sportsmanDto.phone))
    }
    
    @Test
    fun delete() {
        val sportsman = sportsmanService.create(DevDbInit.sportsman(emptyList(), emptyList()))
        
        mockMvc
            .perform(delete("$URL_TEMPLATE/${sportsman.id}"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("id").value(sportsman.id?.toInt()))
            .andExpect(jsonPath("fullName").value(sportsman.fullName))
            .andExpect(jsonPath("alias").value(sportsman.alias))
            .andExpect(jsonPath("notes").value(sportsman.notes))
            .andExpect(jsonPath("phone").value(sportsman.phone))
    }
    
    companion object {
        const val URL_TEMPLATE = "/sportsman"
        
        fun sportsmanDto() = SportsmanDto().apply {
            fullName = "Anton Timchenko"
            alias = "AntAndTim"
            notes = "Very strong gay"
            phone = "+79999999999"
        }
    }
}
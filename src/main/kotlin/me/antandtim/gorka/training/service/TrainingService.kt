package me.antandtim.gorka.training.service

import me.antandtim.gorka.common.service.DefaultCrudService
import me.antandtim.gorka.training.model.Training
import me.antandtim.gorka.training.repository.TrainingRepository
import org.springframework.stereotype.Service

@Service
class TrainingService(private val trainingRepository: TrainingRepository) :
    DefaultCrudService<TrainingRepository, Training> {

    override fun repository(): TrainingRepository {
        return trainingRepository
    }
}
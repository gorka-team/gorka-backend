package me.antandtim.gorka.telegram.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.TelegramBotsApi

@Service
class BotStarter(private val bot: GorkaBot) : ApplicationRunner {
    
    private val logger: Logger = LoggerFactory.getLogger(BotStarter::class.java)
    
    override fun run(args: ApplicationArguments?) {
        try {
            TelegramBotsApi().registerBot(bot)
            logger.info("Bot ${bot.botUsername} started.")
        } catch (e: Exception) {
            logger.error("Error occurred while starting bot!", e)
        }
    }
    
}
package me.antandtim.gorka.news.repository

import me.antandtim.gorka.news.model.News
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface NewsRepository : JpaRepository<News, Long>
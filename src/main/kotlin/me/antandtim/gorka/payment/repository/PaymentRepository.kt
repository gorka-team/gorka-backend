package me.antandtim.gorka.payment.repository

import me.antandtim.gorka.payment.model.Payment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.Instant
import java.time.temporal.ChronoUnit

@Repository
interface PaymentRepository : JpaRepository<Payment, Long> {
    
    fun findAllByIsPaidFalseAndPaymentDateBetweenAndWasNotifiedFalse(
        startDate: Instant,
        endDate: Instant
    ): List<Payment>
    
    fun findAllByIsPaidFalseAndPaymentDateBeforeAndLastNotificationBefore(
        paymentDate: Instant = Instant.now(),
        lastNotification: Instant = Instant.now().minus(1, ChronoUnit.DAYS)
    ): List<Payment>
}
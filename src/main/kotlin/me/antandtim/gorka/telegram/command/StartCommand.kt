package me.antandtim.gorka.telegram.command

import me.antandtim.gorka.telegram.command.CommandEnum.CommandName.start
import me.antandtim.gorka.telegram.model.TelegramChat
import me.antandtim.gorka.telegram.service.TelegramChatService
import me.antandtim.telegram.api.annotation.Command
import me.antandtim.telegram.api.annotation.UpdateHandler
import me.antandtim.telegram.api.domain.TelegramConnection
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Component
@Command(start)
class StartCommand(private val telegramChatService: TelegramChatService) {
    
    @UpdateHandler
    fun onUpdate(telegramConnection: TelegramConnection<SendMessage, Unit>) =
        telegramConnection.run {
            telegramChatService.create(TelegramChat().apply {
                chatId = update.message.chatId
                alias = update.message.from.userName
            })
            
            send(
                SendMessage()
                    .setChatId(update.message.chatId)
                    .setText("Здарова, бандит! Тебя заметила кочка, ${update.message.chat.firstName}")
            )
        }
}
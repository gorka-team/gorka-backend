package me.antandtim.gorka.news.model

import java.time.Instant

class NewsDto {
    
    var id: Long? = null
    
    var title: String? = null
    
    var body: String? = null
    
    var timestamp: Instant? = null
}
package me.antandtim.gorka.config

import me.antandtim.gorka.GorkaScan
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling

@Configuration
@EnableScheduling
@EntityScan(basePackageClasses = [GorkaScan::class])
@ComponentScan(basePackageClasses = [GorkaScan::class])
@EnableJpaRepositories(basePackageClasses = [GorkaScan::class])
class AppConfig
package me.antandtim.gorka.sportsman.mapper

import me.antandtim.gorka.common.mapper.EntityMapper
import me.antandtim.gorka.sportsman.model.Sportsman
import me.antandtim.gorka.sportsman.model.SportsmanDto
import me.antandtim.gorka.training.model.Training
import me.antandtim.gorka.training.repository.TrainingRepository
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired

@Mapper(componentModel = "spring")
abstract class SportsmanMapper : EntityMapper<Sportsman, SportsmanDto> {

    @Autowired
    private lateinit var trainingRepository: TrainingRepository

    @JvmName("mapFromTraining")
    fun map(value: List<Training>): List<Long> {
        return value.mapNotNull { it.id }
    }

    @JvmName("mapToTraining")
    fun map(value: List<Long>?): List<Training> {
        if (value == null) return emptyList()
        return trainingRepository.findAllById(value)
    }
}
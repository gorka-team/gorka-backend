package me.antandtim.gorka.training.model

import java.time.Instant

class TrainingListDto {

    var id: Long? = null

    var sportsmen: List<String>? = null

    var startTime: Instant? = null

    var notes: String? = null
}
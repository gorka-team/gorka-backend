package me.antandtim.gorka.common.controller

import me.antandtim.gorka.common.mapper.EntityMapper
import me.antandtim.gorka.common.model.CountableResponse
import me.antandtim.gorka.common.service.DefaultCrudService
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.web.bind.annotation.*

interface CrudController<Dto : Any> {

    @GetMapping
    fun getList(@RequestParam(value = "id", required = false) ids: List<Long>?): CountableResponse<List<Dto>>

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: Long): Dto

    @PostMapping
    fun create(@RequestBody dto: Dto): Dto

    @PutMapping("/{id}")
    fun update(@RequestBody dto: Dto, @PathVariable id: String): Dto

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): Dto
}

class DefaultCrudController<
        Dto : Any,
        Entity : Any,
        Service : DefaultCrudService<Repository, Entity>,
        Repository : JpaRepository<Entity, Long>,
        Mapper : EntityMapper<Entity, Dto>
        >
    (private val service: Service, private val mapper: Mapper) : CrudController<Dto> {

    override fun getList(ids: List<Long>?) =
        if (ids == null || ids.isEmpty()) {
            val list = service.getList().map { mapper.dto(it) }
            CountableResponse(list.size.toLong(), list)
        } else {
            val list = service.getMany(ids).map { mapper.dto(it) }
            CountableResponse(list.size.toLong(), list)
        }

    override fun getOne(id: Long): Dto {
        return mapper.dto(service.getOne(id) ?: throw NoSuchElementException("There is no element with id: $id"))
    }

    override fun create(dto: Dto): Dto {
        return mapper.dto(service.create(mapper.entity(dto)))
    }

    override fun update(dto: Dto, id: String): Dto {
        // Right now ignoring id, as it is already in dto
        return mapper.dto(service.update(mapper.entity(dto)))
    }

    override fun delete(id: Long): Dto {
        try {
            return mapper.dto(service.delete(id))
        } catch (e: NoSuchElementException) {
            throw NoSuchElementException("You have tried to delete the element which does not exist")
        }
    }
}
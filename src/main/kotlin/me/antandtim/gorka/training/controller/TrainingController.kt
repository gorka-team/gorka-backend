package me.antandtim.gorka.training.controller

import me.antandtim.gorka.common.controller.DefaultCrudController
import me.antandtim.gorka.common.model.CountableResponse
import me.antandtim.gorka.training.mapper.TrainingMapper
import me.antandtim.gorka.training.model.TrainingDto
import me.antandtim.gorka.training.service.TrainingService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("training")
class TrainingController(private val trainingService: TrainingService, private val trainingMapper: TrainingMapper) {

    private val defaultCrudController = DefaultCrudController(trainingService, trainingMapper)

    @GetMapping
    fun getList(@RequestParam(value = "id", required = false) ids: List<Long>?) =
        if (ids == null || ids.isEmpty()) {
            val list = trainingService.getList().map { trainingMapper.toListDto(it) }
            CountableResponse(list.size.toLong(), list)
        } else {
            val list = trainingService.getMany(ids).map { trainingMapper.toListDto(it) }
            CountableResponse(list.size.toLong(), list)
        }

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: Long) = defaultCrudController.getOne(id)

    @PostMapping
    fun create(@RequestBody trainingDto: TrainingDto) = defaultCrudController.create(trainingDto)

    @PutMapping("/{id}")
    fun update(@RequestBody trainingDto: TrainingDto, @PathVariable id: String) =
        defaultCrudController.update(trainingDto, id)

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = defaultCrudController.delete(id)
}
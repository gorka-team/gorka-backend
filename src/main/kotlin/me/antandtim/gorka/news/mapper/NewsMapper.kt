package me.antandtim.gorka.news.mapper

import me.antandtim.gorka.common.mapper.EntityMapper
import me.antandtim.gorka.news.model.News
import me.antandtim.gorka.news.model.NewsDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
abstract class NewsMapper : EntityMapper<News, NewsDto>

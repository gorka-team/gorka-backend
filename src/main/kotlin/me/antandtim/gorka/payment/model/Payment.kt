package me.antandtim.gorka.payment.model

import me.antandtim.gorka.sportsman.model.Sportsman
import java.time.Instant
import javax.persistence.*

@Entity
class Payment {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "paymentIdGenerator")
    @SequenceGenerator(name = "paymentIdGenerator", sequenceName = "payment_seq")
    var id: Long? = null
    
    var isPaid: Boolean? = null
    
    var paymentDate: Instant? = null
    
    var amount: Double? = null
    
    var wasNotified: Boolean? = false
    
    var lastNotification: Instant? = null
    
    @ManyToOne
    @JoinTable(
        name = "SPORTSMAN_PAYMENT",
        joinColumns = [JoinColumn(name = "PAYMENT_ID")],
        inverseJoinColumns = [JoinColumn(name = "SPORTSMAN_ID")]
    )
    var sportsman: Sportsman? = null
}
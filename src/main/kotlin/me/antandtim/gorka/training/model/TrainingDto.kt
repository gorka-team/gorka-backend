package me.antandtim.gorka.training.model

import java.time.Instant

class TrainingDto {

    var id: Long? = null

    var sportsmen: List<Long>? = null

    var startTime: Instant? = null

    var notes: String? = null
}
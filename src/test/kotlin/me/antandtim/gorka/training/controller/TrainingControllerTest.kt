package me.antandtim.gorka.training.controller

import com.fasterxml.jackson.databind.ObjectMapper
import me.antandtim.gorka.AbstractSpringTest
import me.antandtim.gorka.config.dev.DevDbInit
import me.antandtim.gorka.sportsman.service.SportsmanService
import me.antandtim.gorka.training.model.TrainingDto
import me.antandtim.gorka.training.service.TrainingService
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.time.Instant

internal class TrainingControllerTest : AbstractSpringTest() {
    
    @Autowired
    private lateinit var objectMapper: ObjectMapper
    
    @Autowired
    private lateinit var sportsmanService: SportsmanService
    
    @Autowired
    private lateinit var trainingService: TrainingService
    
    @BeforeEach
    fun setup() {
        kotlin.runCatching { sportsmanService.delete(1) }
        sportsmanService.create(DevDbInit.sportsman(emptyList(), emptyList()))
    }
    
    @Test
    fun getList() {
        sportsmanService.getOne(1)?.let {
            val training = trainingService.create(DevDbInit.training(it))
            
            mockMvc
                .perform(get(URL_TEMPLATE))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.content.[*].id").value(hasItem(training.id?.toInt())))
                .andExpect(jsonPath("$.content.[*].sportsmen.[*]").value(hasItem("AntAndTim")))
                .andExpect(jsonPath("$.content.[*].notes").value(hasItem(NOTES)))
        }
    }
    
    @Test
    fun getOne() {
        sportsmanService.getOne(1)?.let {
            val training = trainingService.create(DevDbInit.training(it))
            
            mockMvc
                .perform(get("$URL_TEMPLATE/${training.id}"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("id").value(training.id?.toInt()))
                .andExpect(jsonPath("sportsmen.[*]").value(hasItem(1)))
                .andExpect(jsonPath("notes").value(NOTES))
        }
    }
    
    @Test
    fun create() {
        mockMvc
            .perform(
                post(URL_TEMPLATE)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(trainingDto()))
            )
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("sportsmen.[*]").value(hasItem(1)))
            .andExpect(jsonPath("notes").value(NOTES))
    }
    
    @Test
    fun update() {
        sportsmanService.getOne(1)?.let {
            val training = trainingService.create(DevDbInit.training(it))
            
            mockMvc
                .perform(
                    put("$URL_TEMPLATE/${training}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(trainingDto().apply {
                            id = training.id
                            notes = "$NOTES lol"
                        }))
                )
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("id").value(training.id?.toInt()))
                .andExpect(jsonPath("sportsmen.[*]").value(hasItem(1)))
                .andExpect(jsonPath("notes").value("$NOTES lol"))
        }
    }
    
    @Test
    fun delete() {
        sportsmanService.getOne(1)?.let {
            val training = trainingService.create(DevDbInit.training(it))
            
            mockMvc
                .perform(delete("$URL_TEMPLATE/${training.id}"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("id").value(training.id?.toInt()))
                .andExpect(jsonPath("sportsmen.[*]").value(hasItem(1)))
                .andExpect(jsonPath("notes").value(NOTES))
        }
    }
    
    companion object {
        const val URL_TEMPLATE = "/training"
        const val NOTES = "Some gay training"
        
        fun trainingDto() = TrainingDto().apply {
            sportsmen = listOf(1)
            startTime = Instant.now()
            notes = NOTES
        }
    }
}
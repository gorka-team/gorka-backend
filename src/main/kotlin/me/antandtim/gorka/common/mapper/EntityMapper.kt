package me.antandtim.gorka.common.mapper

interface EntityMapper<Entity, Dto> {

    fun dto(entity: Entity): Dto

    fun entity(dto: Dto): Entity
}
package me.antandtim.gorka.training.repository

import me.antandtim.gorka.training.model.Training
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TrainingRepository : JpaRepository<Training, Long>
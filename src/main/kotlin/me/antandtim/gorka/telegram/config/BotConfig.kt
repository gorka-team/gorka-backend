package me.antandtim.gorka.telegram.config

import me.antandtim.telegram.api.config.annotation.EnableTelegramApi
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
@EnableTelegramApi
data class BotConfig (
    
    @Value("\${bot.token}")
    val token: String,
    
    @Value("\${bot.name}")
    val name: String
)
package me.antandtim.gorka.common.service

import org.springframework.data.jpa.repository.JpaRepository

interface DefaultCrudService<Repository : JpaRepository<Entity, Long>, Entity : Any> {

    fun repository(): Repository

    fun getList(): List<Entity> {
        return repository().findAll()
    }

    fun getOne(id: Long): Entity? {
        return repository().findById(id).orElse(null)
    }

    fun getMany(ids: List<Long>): List<Entity> {
        return repository().findAllById(ids)
    }

    fun create(entity: Entity): Entity {
        return repository().save(entity)
    }

    fun update(entity: Entity): Entity {
        return repository().save(entity)
    }

    fun delete(id: Long): Entity {
        val entity = repository().findById(id).orElseThrow()
        repository().delete(entity)
        return entity
    }
}
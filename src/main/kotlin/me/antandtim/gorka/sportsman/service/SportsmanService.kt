package me.antandtim.gorka.sportsman.service

import me.antandtim.gorka.common.service.DefaultCrudService
import me.antandtim.gorka.sportsman.model.Sportsman
import me.antandtim.gorka.sportsman.repository.SportsmanRepository
import org.springframework.stereotype.Service

@Service
class SportsmanService(private val repository: SportsmanRepository) :
    DefaultCrudService<SportsmanRepository, Sportsman> {

    override fun repository(): SportsmanRepository {
        return repository
    }

}
package me.antandtim.gorka.testcommons.testservice

import me.antandtim.gorka.common.service.DefaultCrudService

open class TestService(
        private val repository: TestRepository
) : DefaultCrudService<TestRepository, TestEntity> {
    override fun repository() = repository
}
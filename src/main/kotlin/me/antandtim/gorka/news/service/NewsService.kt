package me.antandtim.gorka.news.service

import me.antandtim.gorka.common.service.DefaultCrudService
import me.antandtim.gorka.news.model.News
import me.antandtim.gorka.news.repository.NewsRepository
import org.springframework.stereotype.Service

@Service
class NewsService(
    private val newsRepository: NewsRepository
) : DefaultCrudService<NewsRepository, News> {
    
    override fun repository() = newsRepository
}